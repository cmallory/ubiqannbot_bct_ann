import logging
import os
import sys
import time
from dataclasses import dataclass, field

import jstyleson
import requests
import structlog
from discord_webhook import DiscordWebhook
from sqlalchemy import Column, MetaData, Table
from sqlalchemy.dialects.sqlite import *
from sqlalchemy.engine import Engine, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


@dataclass(eq=False, order=False)
class Config:
    """
    Config class to populate items for configuration
    """
    filename: str = field(init=False)
    sqlite_db: str = field(init=False)
    webhook_url: str = field(init=False)
    title_keywords: list = field(init=False)
    bearer_token: str = field(init=False)
    root_dir: str = os.path.dirname(__file__) + os.path.sep

    def __post_init__(self):
        if os.path.isfile(self.root_dir + "config.json"):
            try:
                with open(self.root_dir + "config.json") as configfile:
                    config = jstyleson.load(configfile)  # jstyleson allows for non-standard comments in json
                    self.sqlite_db = config["sqlite_db"]
                    self.webhook_url = config["webhook_url"]
                    self.bearer_token = config["bearer_token"]
                    self.title_keywords = config["title_keywords"]
            except:
                logger.exception("Failed to populate config settings")
                exit()
        else:
            logger.fatal("Config file not found")
            exit()


def create_db_table(engine: Engine):
    metadata = MetaData(engine)
    Table("tweets", metadata,
          Column("id", INTEGER, primary_key=True, autoincrement=False),
          Column("tweet", TEXT),
          Column("expanded_url", TEXT)
          )
    metadata.create_all()


class Tweet(Base):
    __tablename__ = "Tweets"
    id = Column(INTEGER, primary_key=True, autoincrement=False)
    tweet = Column(TEXT)
    expanded_url = Column(TEXT)

    @staticmethod
    def save_to_database(entitylist, session):
        try:
            session.add_all(entitylist)
            session.commit()
        except:
            session.rollback()


def check_existing_tweet(session, tweet_id):
    """
    Searches database for existing tweet id
    :param session: Database session
    :param tweet_id: Tweet ID to search for
    :return: Returns a boolean if found or not
    """
    tweet_exists = session.query(Tweet).filter(Tweet.id == tweet_id)[:]
    if len(tweet_exists) > 0:
        return True
    else:
        return False


def removeshortlink(text: str):
    """
    Removes the Twitter short link part of the text, if it exists
    :param text: Tweet text string
    :return: Tweet text with short url removed if it was found
    """
    split = text.split(" ")
    if split[-1].startswith("https://t.co/"):
        text = text.replace(" " + split[-1], "")
    return text


def senddiscordmsg(tweet: Tweet, config: Config):
    """
    Sends the webhook to Discord
    :param tweet: Instance of Tweet class
    :param config: Config instance
    :return: None
    """
    webhook = DiscordWebhook(config.webhook_url)
    webhook.content = f"{tweet.tweet}\n{tweet.expanded_url}"
    webhook.execute()


def gettweet():
    """
    Fetches the latest tweets from Ubiqannbot
    :return: Tweet instance
    """
    tweetlist = []
    url = "https://api.twitter.com/2/tweets/search/recent?query=from:ubiqannbot&tweet.fields=entities"
    try:
        # Use generic catch-all header for better compatibility and less chance of DNS providers blocking the request
        headers = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36", "Authorization": f"Bearer {config.bearer_token}"}
        r = requests.get(url=url, headers=headers)
        if r.status_code == 200:
            rawjson = r.json()
            for item in rawjson["data"]:
                tweet = Tweet(
                    id=item["id"],
                    tweet=removeshortlink(item["text"]),
                    expanded_url=item["entities"]["urls"][0]["expanded_url"]
                )
                if any(x.id == tweet.id for x in tweetlist) is False:
                    tweetlist.append(tweet)
    except:
        pass
    if len(tweetlist) > 0:
        tweetlist.sort(key=lambda x: x.id, reverse=False)
    return tweetlist


if __name__ == "__main__":
    # Logging setup
    logging.basicConfig(
        stream=sys.stdout, level=logging.INFO
    )
    structlog.configure(
        processors=[
            structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M:%S", utc=True),
            structlog.processors.format_exc_info,
            structlog.processors.KeyValueRenderer(
                key_order=["timestamp", "event"],
                drop_missing=True
            ),
        ],
        context_class=structlog.threadlocal.wrap_dict(dict),
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
    )
    logger = structlog.get_logger(__name__)

    # Setup config vars
    config = Config()
    engine = create_engine(f"sqlite:///{config.root_dir}{config.sqlite_db}")
    Session = sessionmaker(bind=engine)
    session = Session()

    # Create new list of tweets
    tweets = gettweet()
    if len(tweets) > 0:
        new_tweets = []
        for tweet in tweets:
            # Search tweet text for keywords
            for keyword in config.title_keywords:
                if keyword.lower() in str(tweet.tweet).lower():
                    create_db_table(engine)
                    if check_existing_tweet(session, tweet.id) is True:
                        # tweet id was found in database
                        logger.info(f"Coin with ID {tweet.id} exists")
                        break
                    else:
                        # tweet id was not found in database
                        new_tweets.append(tweet)
                        senddiscordmsg(tweet, config)
                        logger.info(f"A new coin with ID {tweet.id} was found")
                        time.sleep(1)
                        break
        if len(new_tweets) > 0:
            try:
                Tweet.save_to_database(new_tweets, session)
                logger.info("New tweets saved to database")
            except:
                logger.exception("Failed to save new tweets to database")
        else:
            logger.info("No new coins found")
    else:
        logger.info("No new tweets found")
    session.close()
    exit()
