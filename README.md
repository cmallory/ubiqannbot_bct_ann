# Ubiqannbot Bitcointalk Ann Bot
* Author: cmallory183
* Version: 1.1.0
* Created On: 2020-09-13
* Updated On: 2020-09-14

## Description
This is a webhook for Discord meant to run on a schedule (preferrably every 1m to 5m).  It will scan the UbiqAnnBot on Twitter using the Twitter API v2 and report back the latest tweet given a list of keywords to search in the tweet title.

## Installation
1.  Clone this repo to a location of your choosing
2.  Create a python virtual environment (python 3.7+ using venv is recommended)
3.  Activate virtual environment
4.  Install modules from requirements.txt
5.  Rename `config.json.sample` to `config.json`
6.  Edit `config.json` to include your webhook URL, bearer token for Twitter (requires developer account), optionally change the filename for the sqlite DB (filename without path as file will be saved in script directory), and optionally change the list of keywords to search in the tweet title
7.  Schedule `ubiqannbot_bct_ann.py` to run every 1-5 minutes as desired

*_Note: this has only been tested on Linux, ymmv on Windows_*

## Credits
Original idea and snippets taken from AIOMiner and the project for AIO-BTC (https://github.com/BobbyGR/AIOMiner_Tools)

UbiqAnnBot on Twitter (https://twitter.com/ubiqannbot)

## Changelog
* 1.1.0
    * Switched to sqlite and SQLAlchemy for tweet tracking (allows for tracking of multiple tweets posted at the same time)
    * Added logging
    * Sorted tweets by ascending ID so they are shown in order of posting
    * Added a 1s delay between webhook postings in order to not trigger the Discord rate limit
* 1.0.0
    * Initial release
